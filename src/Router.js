import React from 'react';
import { createStackNavigator } from 'react-navigation';

import Homepage from './screens/Homepage';
import Fame from './screens/Fame';
import Notifications from './screens/Notifications';
import Profile from './screens/Profile';
import Add from './screens/Add';
import Login from './screens/Login';
import inviteCode from './screens/inviteCode';
import Otp from './screens/Otp';
import welcome from './screens/welcome';
import inviteProfile from './screens/inviteProfile';
import addProfileInfo from './screens/addProfileInfo';
import wallet from './screens/wallet';
import myPools from './screens/myPools';
import PoolChat from './screens/PoolChat';
import Wallet from './screens/Wallet';

export default Router = createStackNavigator({
    homepage: {
        screen: Homepage,
        navigationOptions: { header: null }
    },
    fame: {
        screen: Fame,
        navigationOptions: { header: null }
    },
    notifications: {
        screen: Notifications,
        navigationOptions: { header: null }
    },
    profile: {
        screen: Profile,
        navigationOptions: { header: null }
    },
    add: {
        screen: Add,
        navigationOptions: { header: null }
    },
    login: {
        screen: Login,
        navigationOptions: { header: null }
    },
    Otp: {
        screen: Otp,
        navigationOptions: { header: null }
    },
    inviteCode: {
        screen: inviteCode,
        navigationOptions: { header: null }
    },
    welcome: {
        screen: welcome,
        navigationOptions: { header: null }
    },
    inviteProfile: {
        screen: inviteProfile,
        navigationOptions: { header: null }
    },
    addProfileInfo: {
        screen: addProfileInfo,
        navigationOptions: { header: null }
    },
    wallet: {
        screen: wallet,
        navigationOptions: { header: null }
    },
    myPools: {
        screen: myPools,
        navigationOptions: { header: null }
    },
    poolChat: {
        screen: PoolChat,
        navigationOptions: { header: null }
    },
    wallet: {
        screen: Wallet,
        navigationOptions: { header: null }
    },
},{
    initialRouteName: 'welcome'
    // initialRouteName: 'addProfileInfo'
});