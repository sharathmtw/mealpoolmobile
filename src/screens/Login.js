import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity, Alert, FlatList, Keyboard } from 'react-native';
import { Styles, Header, CText, CardView, CButton, CInput } from '../common';
import Utils from '../common/Utils';
import axios from 'axios';
import Config from '../Config';


export default class login extends Component {
    state = {
        selectedTab: '2',
        phNo: '',
        name: '',
        checkbox: false

    }
    onSubmit(id) {
        Keyboard.dismiss();
        if (id) {
            if (id === 'signUp') {
                if (!this.state.name && this.state.name === '') {
                    alert("Please Fill the name");
                } else if (!this.state.phNo && this.state.phNo === '') {
                    alert("Please enter Mobile Number");
                } else if (!Utils.isValidMobile(Number(this.state.phNo))) {
                    alert("Enter a Valid Mobile Number");
                } else if (!this.state.checkbox) {
                    alert("Please Accept our Terms and Conditions");
                } else {
                    console.warn('hello',
                        this.state.name,
                        this.state.phNo,
                        this.state.checkbox
                    )
                    this.signUp(id);
                }
            } else {
                if (!this.state.phNo && this.state.phNo === '') {
                    alert("Please enter Mobile Number")
                } else if (!Utils.isValidMobile(Number(this.state.phNo))) {
                    alert("Enter a Valid Mobile Number");
                } else {
                    this.signIn();
                }
            }
        }
    }

    signUp(id) {
        axios({
            method: 'post',
            url: Config.routes.base + Config.routes.registerUser,
            data:
                {
                    "name": this.state.name,
                    "phone": this.state.phNo,
                    "companyCode": "mtwlabs123"
                }
        }).then((response) => {
            console.warn('response', response)
            if (response.data) {
                if (response.data.status) {
                    Utils.setToken('UserDetails', JSON.stringify(response), function (d) {

                    });
                    this.props.navigation.navigate('Otp', { phNo: this.state.phNo, id:id });
                } else {
                    alert(response.data.message)
                }
            }
        }).catch((error) => {
            console.log(error)
        })
    }

    signIn() {
        axios({
            method: 'get',
            url: Config.routes.base + Config.routes.loginUser + this.state.phNo,
        }).then((response) => {
            console.warn('response', response)
            if (response.data) {
                if (response.data.status) {
                    this.props.navigation.navigate('Otp', { phNo: this.state.phNo });
                } else {
                    alert(response.data.message)
                }
            }
        }).catch((error) => {
            console.log(error)
        })
    }

    render() {
        return (
            <View style={[Styles.flex1, Styles.bgWhite, Styles.padH40]} >
                <View style={[Styles.jCenter, Styles.aitCenter, { width: 180, height: 100, marginTop: 40, alignSelf: 'center' }]} >
                    <Image style={[{ width: 125, height: 85 }]} source={require('../images/logo_green.png')} />
                </View>
                <View style={[Styles.row, Styles.aslCenter, Styles.mTop20]} >
                    <TouchableOpacity onPress={() => this.setState({ selectedTab: '1', phNo: '', name: '', checkbox: false })} style={[this.state.selectedTab === '1' ? Styles.brdBtmWth2 : null, { borderColor: '#60BA72' }]} >
                        <CText cStyle={[Styles.f18, , Styles.padV10, this.state.selectedTab === '1' ? Styles.fWbold : null, { color: '#60BA72' }]} >SIGN UP</CText>
                    </TouchableOpacity>
                    <View style={{ width: 2, backgroundColor: '#60BA72', marginVertical: 10, marginHorizontal: 20 }} >
                    </View>
                    <TouchableOpacity onPress={() => this.setState({ selectedTab: '2', phNo: '', name: '', checkbox: false })} style={[this.state.selectedTab === '2' ? Styles.brdBtmWth2 : null, { borderColor: '#60BA72' }]} >
                        <CText cStyle={[Styles.f18, , Styles.padV10, this.state.selectedTab === '2' ? Styles.fWbold : null, { color: '#60BA72' }]} >SIGN IN</CText>
                    </TouchableOpacity>
                </View>
                <View style={[{ display: this.state.selectedTab === '1' ? 'flex' : 'none' }]} >
                    <View style={[Styles.mTop30, { backgroundColor: '#DEE3E3', borderRadius: 40 }]} >
                        <CInput
                            cStyle={[Styles.marH20, {}]}
                            pColor={'#000'}
                            placeholder={'Name'}
                            onChangeText={(value) => { this.setState({ name: value }) }}
                            value={this.state.name}
                        />
                    </View>
                    <View style={[Styles.marV20, { backgroundColor: '#DEE3E3', borderRadius: 40 }]} >
                        <CInput
                            cStyle={[Styles.marH20, {}]}
                            pColor={'#000'}
                            placeholder={'Mobile Number'}
                            keyboardType={'numeric'}
                            onChangeText={(value) => { this.setState({ phNo: value }) }}
                            value={this.state.phNo}
                        />
                    </View>
                    <View style={[Styles.marV10, Styles.row, {}]} >
                        <TouchableOpacity onPress={() => this.setState({ checkbox: !this.state.checkbox })} style={[{ height: 25, width: 25, borderRadius: 5, backgroundColor: '#DEE3E3', justifyContent: 'center', alignSelf: 'center' }]} >
                            <Image style={{ width: 15, height: 15, alignSelf: 'center', display: this.state.checkbox ? 'flex' : 'none' }} source={require('../images/tick_green.png')} />
                        </TouchableOpacity>
                        <CText cStyle={[Styles.cBlk, Styles.mLt10]} >By signing up I agree to abide the terms &amp; conditions</CText>
                    </View>
                    <CButton onPress={() => this.onSubmit('signUp')} cStyle={[Styles.bgGrn, Styles.marV20, { height: 40, borderRadius: 40 }]} >
                        <CText cStyle={[Styles.cFFF, Styles.f16]} >REQUEST OTP</CText>
                    </CButton>
                </View>
                <View style={[{ display: this.state.selectedTab === '2' ? 'flex' : 'none' }]} >
                    <View style={[Styles.mTop30, { backgroundColor: '#DEE3E3', borderRadius: 40 }]} >
                        <CInput
                            maxLength={10}
                            cStyle={[Styles.marH20, {}]}
                            pColor={'#000'}
                            placeholder={'Mobile Number'}
                            keyboardType={'numeric'}
                            onChangeText={(value) => { this.setState({ phNo: value }) }}
                            value={this.state.phNo}
                        />
                    </View>
                    <CButton onPress={() => this.onSubmit('signIn')} cStyle={[Styles.bgGrn, Styles.marV20, { height: 40, borderRadius: 40 }]} >
                        <CText cStyle={[Styles.cFFF, Styles.f16]} >SUBMIT</CText>
                    </CButton>
                </View>
            </View>
        );
    }
}