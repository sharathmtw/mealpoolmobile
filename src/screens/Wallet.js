import React, { Component } from 'react';
import { Platform, View, TouchableOpacity } from 'react-native';
import { Styles, CText } from '../common';
import RazorpayCheckout from 'react-native-razorpay';

export default class Wallet extends Component {
    componentWillMount() {
        if (Platform.os === 'android') {
            SplashScreen.hide();
        }
    }

    onRazorpay() {
        var options = {
            description: 'Credits towards consultation',
            image: 'https://i.imgur.com/3g7nmJC.png',
            currency: 'INR',
            key: 'rzp_test_1DP5mmOlF5G5ag',
            amount: '5000',
            name: 'foo',
            prefill: {
                email: 'void@razorpay.com',
                contact: '9191919191',
                name: 'Razorpay Software'
            },
            theme: { color: '#F37254' }
        }
        RazorpayCheckout.open(options).then((data) => {
            // handle success
            alert(`Success: ${data.razorpay_payment_id}`);
        }).catch((error) => {
            // handle failure
            alert(`Error: ${error.code} | ${error.description}`);
        });
    }
    render() {
        return (
            <View style={[Styles.flex1, Styles.jCenter, Styles.aitCenter]}>
                <CText> Wallet</CText>
                <TouchableOpacity onPress={() => { this.onRazorpay() }}>
                    <CText> Razorpay</CText>
                </TouchableOpacity>
            </View>);
    }

}