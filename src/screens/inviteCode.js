import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity, Alert, FlatList,Keyboard } from 'react-native';
import { Styles, Header, CText, CardView, CButton, CInput } from '../common';
import Utils from '../common/Utils';
import axios from 'axios';
import Config from '../Config';

export default class inviteCode extends Component {
    state = {
        inviteCode: 'mtwlabs123',
        id:''
    }
    componentWillMount() {
        const self = this;
        Utils.getToken('UserDetails', (uResp, stat) => {
            if (stat) {
                // console.warn(uResp.id);
                self.setState({id:uResp.id});
            }
        });
    }
    onSubmit() {
        Keyboard.dismiss();
        if (this.state.inviteCode === '') {
            alert("Enter a Valid Invite Code")
        } else {
            axios({
                method: 'post',
                url: Config.routes.base + Config.routes.checkCommpanyCode,
                data:
                    {
                        "_id": this.state.id,
                        "companyCode": this.state.inviteCode
                    }
            }).then((response) => {
                console.warn('response', response)
                if (response.data) {
                    if (response.data.status) {
                        this.props.navigation.navigate('inviteProfile');
                    } else {
                        alert(response.data.message)
                    }
                }
            }).catch((error) => {
                console.log(error)
            })
        }
    }
    render() {
        return (
            <View style={[Styles.flex1, Styles.bgWhite, Styles.padH40]} >
                <View style={[Styles.marV30]} >
                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('login') }} >
                        <Image style={[{ width: 25, height: 25 }]} source={require('../images/Arrow_CircleLeft.png')} />
                    </TouchableOpacity>
                </View>
                <View style={[Styles.jCenter, Styles.flex1]} >
                    <View style={[Styles.mTop30, Styles.brdWidth1, { borderColor: '#60BA72', }]} >
                        <CInput
                            cStyle={[Styles.marH20, {}]}
                            pColor={'#60BA72'}
                            placeholder={'Enter Invite Code'}
                            onChangeText={(value) => { this.setState({ inviteCode: value }) }}
                            value={this.state.inviteCode}
                        />
                    </View>
                    <CButton onPress={() => this.onSubmit()} cStyle={[Styles.bgGrn, Styles.marV20, { height: 40, borderRadius: 40 }]} >
                        <CText cStyle={[Styles.cFFF, Styles.f16]} >SUBMIT</CText>
                    </CButton>
                    <TouchableOpacity>
                        <CText cStyle={[Styles.cGrn, Styles.fWbold, Styles.f16]} >Need an invite code?</CText>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}