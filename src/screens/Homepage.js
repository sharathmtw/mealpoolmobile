import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity, Alert, FlatList } from 'react-native';
import { Styles, Header, CText, CardView } from '../common';
import Utils from '../common/Utils';
import FooterTabs from '../common/FooterTabs';
import axios from 'axios';
import Config from '../Config';
import LinearGradient from 'react-native-linear-gradient';

export default class Homepage extends Component {
    state = {
        token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViNmQyZjc5YTdmNTk1MDhkZDIwM2Y5OSIsInBob25lIjo4NjM5NTE5MTUxLCJuYW1lIjoic2hhcmF0aCIsImNvbXBhbnlJZCI6IjViNmQyZDU2N2U3Y2YyMDc4ODhlOGU3NyIsImlhdCI6MTUzMzg4MjMxOX0.nS2rlc4Uez0WII2XBCJIRD1MMomIz43bdLOE5VZFWUE',
        hashData: Utils.getStaticData('hashtags'),
        poolData: Utils.getStaticData('poolData'),
    }

    componentWillMount() {
        const self = this;
        Utils.getToken('UserDetails', (uResp, stat) => {
            if (stat) {
                // console.warn(uResp.id);
                self.setState({token:uResp.token});
            }
        });
        this.getAllPools();
    }
    getAllPools() {
        //console.warn('hello')

        axios({
            method: 'get',
            url: Config.routes.base + Config.routes.getAllActivities,
            headers: { 'token': this.state.token },
        }).then((response) => {
            //console.warn('response', response)
            if (response.data) {
                if (response.data.status) {
                    this.setState({ poolData: response.data.activities })
                    // Alert.alert('',
                    //     response.data.message,
                    //     [
                    //         { text: 'OK', onPress: () => { this.props.navigation.navigate('homepage')} },
                    //     ],
                    //     { cancelable: false }
                    // )
                }
            }
        }).catch((error) => {
            console.log(error)
        })
    }

    renderHashtags(item) {
        return <TouchableOpacity onPress={() => { }}>
            <CText cStyle={[Styles.m15, Styles.cFFF]}>{item.tagName}</CText>
        </TouchableOpacity>
    }

    renderPoolData(item) {
        return <CardView
            hashTag={'#' + item.tag}
            imgClick={() => { this.props.navigation.navigate('profile') }}
            imgSource={require('../images/icons/pic.png')}
        >
            <TouchableOpacity onPress={()=>{this.props.navigation.navigate('poolChat',{data:item})}}>
                <CText cStyle={[Styles.fWbold, Styles.f16, Styles.mTop5]}>{item.host.name}</CText>
                <CText><CText cStyle={{ color: "#60BA72" }}>{'#' + item.tag + ":"} </CText>{item.dishName}</CText>
            </TouchableOpacity>
        </CardView>
    }

    render() {
        return (
            <View style={[Styles.flex1, Styles.bgFFF]}>
                <Header
                    picVisible={true}
                    // picSource={require('../images/menu-white.png')}
                    title="MEAL POOL"
                    searchVisible={true}
                    notifVisible={true}
                    notifClick={() => { this.props.navigation.navigate('notifications') }}
                />
                <View style={[{ height: 50 }, Styles.row, Styles.aitCenter]}>
                    <LinearGradient
                        style={{ backgroundColor: 'transparent', flex: 1 }}
                        colors={['#F5A723', '#60BA72']}
                        start={{ x: 0.0, y: 0.5 }} end={{ x: 1.0, y: 0.5 }}
                    >
                        <FlatList
                            horizontal={true}
                            data={this.state.hashData}
                            keyExtractor={(item, iVal) => iVal.toString()}
                            renderItem={({ item, index }) => this.renderHashtags(item, index)}
                            extradata={this.state}
                        />
                    </LinearGradient>
                </View>

                <View style={[Styles.flex1]}>
                    <FlatList
                        data={this.state.poolData}
                        keyExtractor={(item, iVal) => iVal.toString()}
                        renderItem={({ item, index }) => this.renderPoolData(item, index)}
                        extradata={this.state}
                    />
                </View>
                <FooterTabs
                    fameClick={() => { this.props.navigation.navigate('fame') }}
                    addClick={() => { this.props.navigation.navigate('add', { screenName: 'homepage' }) }}
                />
            </View>
        );
    }
}
