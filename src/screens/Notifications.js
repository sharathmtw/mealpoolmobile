import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity, Alert, FlatList } from 'react-native';
import { Styles, Header, CText, CardView } from '../common';
import Utils from '../common/Utils';
import FooterTabs from '../common/FooterTabs';

export default class Homepage extends Component {
    state = {
        notifData: Utils.getStaticData('notifData'),
    }
    renderHashtags(item) {
        return <TouchableOpacity onPress={() => { }}>
            <CText cStyle={[Styles.m15, Styles.cFFF]}>{item.tagName}</CText>
        </TouchableOpacity>
    }
    renderNotifData(item) {
        return <CardView cStyle={[Styles.p20]}>
            <CText cStyle={[Styles.f14, Styles.mTop5]}>{item.message}</CText>
            <CText cStyle={[Styles.f12, Styles.mTop5]}>{item.timeStamp}</CText>
        </CardView>
    }

    render() {
        return (
            <View style={[Styles.flex1, Styles.bgFFF]}>
                <Header
                    picVisible={true}
                    title="Notifications"
                    notifVisible={true}
                />
                <View style={[Styles.flex1]}>
                    <FlatList
                        data={this.state.notifData}
                        keyExtractor={(item, iVal) => iVal.toString()}
                        renderItem={({ item, index }) => this.renderNotifData(item, index)}
                        extradata={this.state}
                    />
                </View>
                <FooterTabs
                    homeClick={() => { this.props.navigation.navigate('homepage') }}
                    addClick={()=>{this.props.navigation.navigate('add',{screenName:'notifications'})}}
                />
            </View>
        );
    }
}
