import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity, Alert, FlatList } from 'react-native';
import { Styles, Header, CText, CardView, CButton, CInput } from '../common';
import Utils from '../common/Utils';
import axios from 'axios';
import Config from '../Config';


export default class Profile extends Component {
    state = {
        token:
            'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViNmQyZjc5YTdmNTk1MDhkZDIwM2Y5OSIsInBob25lIjo4NjM5NTE5MTUxLCJuYW1lIjoic2hhcmF0aCIsImNvbXBhbnlJZCI6IjViNmQyZDU2N2U3Y2YyMDc4ODhlOGU3NyIsImlhdCI6MTUzMzg4MjMxOX0.nS2rlc4Uez0WII2XBCJIRD1MMomIz43bdLOE5VZFWUE',
        onCloseScreenName: '',
        selectedTab: 'm',
        dishName: '',
        selectedMealType: '',
        noOfPeople: 0,
        mealTypes: Utils.getStaticData('mealTypes'),
        selectedDay: ''

    }
    componentWillMount() {
        if (this.props.navigation.state.params.screenName) {
            this.setState({ onCloseScreenName: this.props.navigation.state.params.screenName });
        }
    }
    showMealTypes() {
        if (this.state.mealTypes) {
            var arr = [];
            let a = this.state.mealTypes;
            for (let i = 0; i < a.length; i++) {
                arr.push(
                    <TouchableOpacity onPress={() => this.onClickMealTypes(a[i])} key={i} style={[Styles.flex1, Styles.mTop15, Styles.aitCenter, Styles.jCenter, Styles.brdRad4, Styles.mLt3, { backgroundColor: a[i].selected ? '#C8E6CE' : '#F6F6F6' }]} >
                        <CText cStyle={[Styles.cBlk, Styles.marV10]}>{a[i].name}</CText>
                    </TouchableOpacity>
                );
            }
        }
        return arr;
    }
    onClickMealTypes(data) {
        if (data) {
            let a = this.state.mealTypes;
            let arr = [];
            for (let i = 0; i < a.length; i++) {
                if (data.id === a[i].id) {
                    a[i].selected = true;
                    this.setState({ selectedMealType: a[i].name });
                    arr.push(a[i]);
                } else {
                    a[i].selected = false;
                    arr.push(a[i]);
                }
            }
            this.setState({ mealTypes: arr });
        }
    }
    onSubmit(id) {
        if (id) {
            if (id === 'm') {
                if (!this.state.dishName && this.state.dishName.length === 0) {
                    alert("Please enter name of the Dish");
                } else if (this.state.selectedDay === '') {
                    alert("Please select a Day");
                } else if (this.state.noOfPeople === 0) {
                    alert("Please increase the No.of people");
                } else if (this.state.selectedMealType === '') {
                    alert("Please choose a meal type");
                } else {
                    this.addPool();
                }
            } else {
                if (!this.state.dishName && this.state.dishName.length === 0) {
                    alert("Please enter name of the Dish");
                }
            }
        }
    }

    addPool() {
        console.log('response',
            this.state.token,
            this.state.dishName,
            this.state.noOfPeople,
            this.state.selectedMealType
        )

        axios({
            method: 'post',
            url: Config.routes.base + Config.routes.addPool,
            headers: { 'token': this.state.token },
            data: {
                "dishName": this.state.dishName,
                "date": "2018-08-10",
                "maxPeople": this.state.noOfPeople,
                "tag": this.state.selectedMealType
            }

        }).then((response) => {
            console.log('response', response)
            if (response.data) {
                if (response.data.status) {
                    Alert.alert('',
                        response.data.message,
                        [
                            { text: 'OK', onPress: () => { this.props.navigation.navigate('homepage')} },
                        ],
                        { cancelable: false }
                    )
                }
            }
        }).catch((error) => {
            console.log(error)
        })
    }
    render() {
        return (
            <View style={[Styles.flex1, Styles.bgFFF]}>
                <View style={[Styles.marH20]}>
                    <View style={[Styles.mTop30]}>
                        <TouchableOpacity style={[{ height: 30, width: 30 }]} onPress={() => { this.props.navigation.goBack() }}>
                            <Image style={{width:25,height:25}} source={require('../images/cross_ash.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={[Styles.row, Styles.mTop30]} >
                        <TouchableOpacity onPress={() => this.setState({ selectedTab: 'm', onCloseScreenName: '', dishName: '', mealTypes: Utils.getStaticData('mealTypes'), selectedMealType: '', noOfPeople: 0, selectedDay: '' })} style={[Styles.flex1, Styles.aitCenter, Styles.jCenter, Styles.padH15, Styles.padV5, Styles.brdWidthDot7, { borderColor: '#60BA72', backgroundColor: this.state.selectedTab === 'm' ? '#60BA72' : '#FFF', borderBottomLeftRadius: 5, borderTopLeftRadius: 5 }]}>
                            <CText cStyle={[Styles.fWbold, Styles.f18, { color: this.state.selectedTab === 'm' ? '#FFF' : '#60BA72' }]}>MAKE</CText>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.setState({ selectedTab: 'r', onCloseScreenName: '', mealTypes: Utils.getStaticData('mealTypes'), dishName: '', selectedMealType: '', noOfPeople: 0, selectedDay: '' })} style={[Styles.flex1, Styles.aitCenter, Styles.jCenter, Styles.padH15, Styles.padV5, Styles.brdWidthDot7, { borderColor: '#60BA72', backgroundColor: this.state.selectedTab === 'r' ? '#60BA72' : '#FFF', borderBottomRightRadius: 5, borderTopRightRadius: 5 }]}>
                            <CText cStyle={[Styles.fWbold, Styles.f18, { color: this.state.selectedTab === 'r' ? '#FFF' : '#60BA72' }]}>REQUEST</CText>
                        </TouchableOpacity>
                    </View>
                    <View style={[Styles.mTop30, { display: this.state.selectedTab === 'm' ? 'flex' : 'none' }]} >
                        <CText cStyle={[Styles.cBlk]}>Name of Dish:</CText>
                        <CInput cStyle={[Styles.brdrBtm, { height: 80 }]} onChangeText={(value) => { this.setState({ dishName: value }) }} value={this.state.dishName} multiline={true} />
                        <View style={[Styles.marH20]} >
                            <View style={[Styles.jSpaceBet, Styles.row, Styles.marV15]}>
                                <TouchableOpacity onPress={() => this.setState({ selectedDay: 'Today' })} style={[Styles.row, Styles.flex1, { borderWidth: 1, borderColor: '#C8E6CE', borderRadius: 25, backgroundColor: this.state.selectedDay === 'Today' ? '#C8E6CE' : '#FFF' }]} >
                                    <View style={[Styles.aslCenter, Styles.mLt10, Styles.bgWhite, { width: 15, height: 15, borderRadius: 15, borderWidth: 1, borderColor: '#C8E6CE' }]}>
                                        <Image style={{ width: 10, height: 10, alignSelf: 'center', display: this.state.selectedDay === 'Today' ? 'flex' : 'none' }} source={require('../images/tick-icon.png')} />
                                    </View>
                                    <CText cStyle={[Styles.cBlk, Styles.marV10, Styles.mLt20]}>Today</CText>
                                </TouchableOpacity>
                                <View style={{ flex: 0.4 }}>

                                </View>
                                <TouchableOpacity onPress={() => this.setState({ selectedDay: 'Tomorrow' })} style={[Styles.row, Styles.flex1, { borderWidth: 1, borderColor: '#C8E6CE', borderRadius: 25, backgroundColor: this.state.selectedDay === 'Tomorrow' ? '#C8E6CE' : '#FFF' }]} >
                                    <View style={[Styles.aslCenter, Styles.mLt10, Styles.bgWhite, { width: 15, height: 15, borderRadius: 15, borderWidth: 1, borderColor: '#C8E6CE' }]}>
                                        <Image style={{ width: 10, height: 10, alignSelf: 'center', display: this.state.selectedDay === 'Tomorrow' ? 'flex' : 'none' }} source={require('../images/tick-icon.png')} />
                                    </View>
                                    <CText cStyle={[Styles.cBlk, Styles.marV10, Styles.mLt20]}>Tomorrow</CText>
                                </TouchableOpacity>
                            </View>
                            <View style={[Styles.row, Styles.mTop10, Styles.brdWidthDot7, Styles.padH15, Styles.brdRad8, Styles.jSpaceBet, { borderColor: '#C8E6CE' }]} >
                                <TouchableOpacity onPress={() => { this.setState({ noOfPeople: this.state.noOfPeople === 0 ? 0 : Number(this.state.noOfPeople) - 1 }) }}>
                                    <CText cStyle={[Styles.fWbold, Styles.f30, { color: '#C8E6CE' }]} >-</CText>
                                </TouchableOpacity>
                                <CText cStyle={[Styles.fWbold, Styles.aslCenter, { color: '#C8E6CE' }]} >{this.state.noOfPeople} PEOPLE</CText>
                                <TouchableOpacity onPress={() => { this.setState({ noOfPeople: this.state.noOfPeople + 1 }) }}>
                                    <CText cStyle={[Styles.fWbold, Styles.f30, { color: '#C8E6CE' }]} >+</CText>
                                </TouchableOpacity>
                            </View>
                            <View style={[Styles.row, Styles.mTop10, Styles.marV15]} >
                                {this.showMealTypes()}
                            </View>
                        </View>
                        <CButton onPress={() => { this.onSubmit('m') }} cStyle={[Styles.mTop15, Styles.bgGrn, Styles.brdRad5, Styles.padV8]}>
                            <CText cStyle={[Styles.cFFF, Styles.fWbold]} >POST POOL</CText>
                        </CButton>
                    </View>
                    <View style={[Styles.mTop30, { display: this.state.selectedTab === 'r' ? 'flex' : 'none' }]} >
                        <CText cStyle={[Styles.cBlk]}>Name of Dish:</CText>
                        <CInput cStyle={[Styles.brdrBtm, { height: 160 }]} onChangeText={(value) => { this.setState({ dishName: value }) }} value={this.state.dishName} multiline={true} />
                        <CButton onPress={() => { this.onSubmit('r') }} cStyle={[Styles.mTop15, Styles.bgGrn, Styles.brdRad5, Styles.padV8]}>
                            <CText cStyle={[Styles.cFFF, Styles.fWbold]} >POST REQUEST</CText>
                        </CButton>
                    </View>
                </View>
            </View>
        );
    }
}