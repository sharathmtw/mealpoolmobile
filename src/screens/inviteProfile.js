import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity, Alert, FlatList } from 'react-native';
import { Styles, Header, CText, CardView, CButton, CInput } from '../common';
import Utils from '../common/Utils';

export default class inviteCode extends Component {

    render() {
        return (
            <View style={[Styles.flex1, Styles.bgWhite, Styles.padH40]} >
                <View style={[Styles.marV30]} >
                    <TouchableOpacity onPress={() => { this.props.navigation.goBack() }} >
                        <Image style={[{ width: 25, height: 25 }]} source={require('../images/Arrow_CircleLeft.png')} />
                    </TouchableOpacity>
                </View>
                <View style={[Styles.padH20, Styles.marV40, Styles.jSpaceArd, Styles.flex1]} >
                    <CText cStyle={[Styles.cBlk, Styles.aslCenter]} >Shanti invited you to join</CText>
                    <Image style={[Styles.aslCenter, { width: 125, height: 125 }]} source={require('../images/inviteProfilePic.png')} />
                    <View style={[Styles.aitCenter]} >
                        <CText cStyle={[Styles.cGrn, Styles.f20, Styles.fWbold, Styles.aslCenter]} >TinMen Office</CText>
                        <CText cStyle={[Styles.cGrn, Styles.f10, Styles.aslCenter]} >MEALPOOL</CText>
                    </View>
                    <View style={[Styles.marH40, { backgroundColor: '#DEE3E3', borderRadius: 40 }]} >
                        <CText cStyle={[Styles.cOng, Styles.marV10, Styles.aslCenter]} >25 Members</CText>
                    </View>
                    <CText cStyle={[Styles.aslCenter]} >Are you sure you want to join this mealpool?</CText>
                    <CButton onPress={() => this.props.navigation.navigate('addProfileInfo')} cStyle={[Styles.bgGrn, { height: 40, borderRadius: 40 }]} >
                        <CText cStyle={[Styles.cFFF, Styles.f16]} >YES</CText>
                    </CButton>
                    <TouchableOpacity onPress={() => { this.props.navigation.goBack() }} >
                        <CText cStyle={[Styles.cBlk, Styles.aslCenter]} >Cancel</CText>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}