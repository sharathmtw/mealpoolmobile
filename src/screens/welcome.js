import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity, Alert, FlatList } from 'react-native';
import { Styles, Header, CText, CardView, CButton, CInput } from '../common';
import Utils from '../common/Utils';

export default class welcome extends Component {
    onClickNext(){
        this.props.navigation.navigate('login');
    }
    render() {
        return (
            <View style={[Styles.flex1, Styles.bgWhite]} >
                <View style={[Styles.flex1]} >
                    <Image style={[{ width: undefined, height: undefined, flex: 1 }]} source={require('../images/homeScreenImg.png')} />
                </View>
                <View style={[Styles.bgGrn,Styles.padH40, { flex: 1.5 }]} >
                    <View style={[ Styles.jCenter]} >
                    <Image style={[Styles.mTop30,Styles.aslCenter,{width:150,height:120}]} source={require('../images/logoFFF.png')} />
                    <View style={[Styles.marV20,Styles.aslCenter,Styles.aitCenter]} >
                        <CText cStyle={[Styles.cFFF, Styles.f16]} >Find out what your friends</CText>
                        <CText cStyle={[Styles.cFFF, Styles.f16]} >are cooking</CText>
                        </View>
                        <CButton onPress={() => this.onClickNext()} cStyle={[Styles.bgGrn, Styles.marV20,Styles.brdWidth1,Styles.marH20, { height: 40, borderRadius: 40,borderColor:'#FFF' }]} >
                            <CText cStyle={[Styles.cFFF, Styles.f16]} >NEXT</CText>
                        </CButton>
                    </View>
                </View>
            </View>
        );
    }
}