import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity, Alert, FlatList, ScrollView } from 'react-native';
import { Styles, Header, CText, CardView, CButton } from '../common';
import Utils from '../common/Utils';
import FooterTabs from '../common/FooterTabs';

export default class Profile extends Component {

    render() {
        return (
            <View style={[Styles.flex1, Styles.bgWhite]} >
                <View style={[]} >
                    <ScrollView>
                        <View style={[Styles.jSpaceBet, Styles.row, Styles.marV40, Styles.aitCenter, Styles.padH40]} >
                            <TouchableOpacity onPress={() => { this.props.navigation.goBack() }} >
                                <Image style={[{ width: 25, height: 25 }]} source={require('../images/Arrow_CircleLeft.png')} />
                            </TouchableOpacity>
                            <View style={[Styles.aslCenter, Styles.jCenter, { width: 140, height: 140, borderRadius: 140, backgroundColor: '#F6F6F6' }]}>
                            </View>
                            <TouchableOpacity onPress={() => { this.props.navigation.goBack() }} >
                                <Image style={[{ width: 30, height: 20 }]} source={require('../images/Edit.png')} />
                            </TouchableOpacity>
                        </View>
                        <CText cStyle={[Styles.fWbold, Styles.f30, Styles.aslCenter, Styles.cBlk]} >Pallavi Laxmikanth</CText>
                        <View style={[Styles.row, Styles.aslCenter, Styles.marV15]} >
                            <Image style={[{ width: 16, height: 20 }]} source={require('../images/locationBlk.png')} />
                            <CText cStyle={[Styles.cAsh, Styles.mLt5, Styles.f18]} >Cybergateway</CText>
                        </View>
                        <View style={[Styles.aslCenter, Styles.marV10, Styles.aitCenter, { backgroundColor: '#F6F6F6', width: 40 }]} >
                            <CText cStyle={[Styles.p5, Styles.cBlk]} >BIO</CText>
                        </View>
                        <CText cStyle={[Styles.cBlk, Styles.f18, Styles.aslCenter, Styles.mBtm30, Styles.marH30]} >I love making Biryanis and Mughalai dishes</CText>
                        <View style={[Styles.bgGrn, Styles.padH60, Styles.aitCenter, Styles.jSpaceArd, Styles.row, { height: 90 }]} >
                            <View style={[Styles.aslCenter, Styles.aitCenter, Styles.jCenter]} >
                                <CText cStyle={[Styles.cFFF, Styles.f18]} >FAME</CText>
                                <CText cStyle={[Styles.cFFF, Styles.f18, Styles.mTop5]} >1900</CText>
                            </View>
                            <View style={[Styles.aslCenter, Styles.aitCenter, Styles.jCenter]} >
                                <CText cStyle={[Styles.cFFF, Styles.f18]} >FOLLOWERS</CText>
                                <CText cStyle={[Styles.cFFF, Styles.f18, Styles.mTop5]} >127</CText>
                            </View>
                        </View>
                        <View style={[]}>
                            <CText cStyle={[Styles.cBlk, Styles.marH30, Styles.mTop15]} >Info</CText>
                            <View style={[{ borderBottomColor: '#F6F6F6', borderBottomWidth: 1.5 }]} >
                                <CText cStyle={[Styles.marV20, Styles.cBlk, Styles.marH30, Styles.fWbold]} >+91 9788668543</CText>
                            </View>
                            <View style={[{ borderBottomColor: '#F6F6F6', borderBottomWidth: 1.5 }]} >
                                <CText cStyle={[Styles.marV20, Styles.cBlk, Styles.marH30, Styles.fWbold]} >pallavi@gmail.com</CText>
                            </View>
                            <TouchableOpacity onPress={()=>{this.props.navigation.navigate('wallet')}} style={[{ backgroundColor: '#F6F6F6', borderBottomColor: '#9B9B9B', borderBottomWidth: 1.1 }]} >
                                <View style={[Styles.m20, Styles.row, Styles.jSpaceBet]} >
                                    <View style={[Styles.row]} >
                                        <Image style={[{ width: 25, height: 25 }]} source={require('../images/walletBlk.png')} />
                                        <CText cStyle={[Styles.cBlk, Styles.fWbold, Styles.mLt20]} >Wallet</CText>
                                    </View>
                                    <View style={[{ borderColor: '#9B9B9B', borderWidth: 1.1, borderRadius: 10 }]} >
                                        <CText cStyle={[Styles.cBlk, { margin: 7 }]} >Rs.1000</CText>
                                    </View>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={()=>{this.props.navigation.navigate('myPools')}} style={[{ backgroundColor: '#F6F6F6', borderBottomColor: '#9B9B9B', borderBottomWidth: 1.1 }]} >
                                <View style={[Styles.m20, Styles.row]} >
                                        <Image style={[{ width: 25, height: 25 }]} source={require('../images/chatLogo.png')} />
                                        <CText cStyle={[Styles.cBlk, Styles.fWbold, Styles.mLt20]} >My Pools</CText>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={[{ backgroundColor: '#F6F6F6', borderBottomColor: '#9B9B9B', borderBottomWidth: 1.1 }]} >
                                <View style={[Styles.m20, Styles.row]} >
                                        <Image style={[{ width: 25, height: 30 }]} source={require('../images/bookmarkBlk.png')} />
                                        <CText cStyle={[Styles.cBlk, Styles.fWbold, Styles.mLt20]} >Bookmarked Comments</CText>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={[{ backgroundColor: '#F6F6F6', borderBottomColor: '#9B9B9B', borderBottomWidth: 1.1 }]} >
                                <View style={[Styles.m20, Styles.row]} >
                                        <Image style={[{ width: 26, height: 30 }]} source={require('../images/lockBlk.png')} />
                                        <CText cStyle={[Styles.cBlk, Styles.fWbold, Styles.mLt20]} >Privacy &amp; Security</CText>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={[{ backgroundColor: '#F6F6F6', borderBottomColor: '#9B9B9B', borderBottomWidth: 1.1 }]} >
                                <View style={[Styles.m20, Styles.row]} >
                                        <Image style={[{ width: 26, height: 30 }]} source={require('../images/powerBlk.png')} />
                                        <CText cStyle={[Styles.cBlk, Styles.fWbold, Styles.mLt20]} >Logout</CText>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </View>
            </View>
        );
    }
}

