import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity, Alert, FlatList, Keyboard } from 'react-native';
import { Styles, Header, CText, CardView, CButton, CInput, PasscodeTextInput } from '../common';
import Utils from '../common/Utils';
import axios from 'axios';
import Config from '../Config';

export default class Otp extends Component {
    state = {
        Otp1: '',
        Otp2: '',
        Otp3: '',
        Otp4: '',
        // Otp5: '',
        // Otp6: '',
        phNo:'',
        otp:'',
        fromId:''
    }

    componentWillMount(){
        this.setState({phNo: this.props.navigation.state.params.phNo, fromId:this.props.navigation.state.params.id},()=>{
            console.log('phNo',this.state.phNo);
        })
    }

    onSubmit() {
        // || this.state.Otp5===''|| this.state.Otp6===''
        if(this.state.Otp1==='' || this.state.Otp2==='' || this.state.Otp3==='' || this.state.Otp4===''){
            alert("Enter a Valid Otp");
        }else{
            this.setState({otp:this.state.Otp1+this.state.Otp2+this.state.Otp3+this.state.Otp4},()=>{
        console.warn(this.state.otp,"otp");
                this.submitOtp();
            })

            // this.props.navigation.navigate('homepage');
        }
    }
    submitOtp(){
        Keyboard.dismiss();
        axios({
            method: 'post',
            url: Config.routes.base + Config.routes.verifyOtp,
            data:
            {
                "phone": this.state.phNo,
                "otp": this.state.otp
            }
            
        }).then((response) => {
            console.warn('response', response)
            if (response.data) {
                if (response.data.status) {
                    Utils.setToken('UserDetails',response.data,(tResp,tStat)=>{
                        if(tStat){
                            if(this.state.fromId!==''){
                                this.props.navigation.navigate('inviteCode');

                            }else{
                            this.props.navigation.navigate('homepage');
                                
                            }
                        }
                    })
                }else{
                    alert(response.data.message)
                }
            }
        }).catch((error) => {
            console.log(error)
        })
    }
resendOtp(){
    axios({
        method: 'get',
        url: Config.routes.base + Config.routes.loginUser + this.state.phNo,
    }).then((response) => {
        console.warn('response', response)
        if (response.data) {
            if (response.data.status) {
                // this.props.navigation.navigate('Otp', { phNo: this.state.phNo });
            } else {
                alert(response.data.message)
            }
        }
    }).catch((error) => {
        console.log(error)
    })
}
    render() {
        return (
            <View style={[Styles.flex1, Styles.bgWhite, Styles.padH40]} >
                <View style={[Styles.marV30]} >
                    <TouchableOpacity onPress={()=>{this.props.navigation.goBack()}} >
                        <Image style={[{ width: 25, height: 25 }]} source={require('../images/Arrow_CircleLeft.png')} />
                    </TouchableOpacity>
                </View>
                <View style={[Styles.jCenter, Styles.aslCenter, Styles.flex1]} >
                    <View style={[Styles.jCenter, Styles.aslCenter]} >
                        <CText>Enter the code you have received by</CText>
                        <CText>SMS in order to verify your account</CText>
                        <View style={[Styles.row, Styles.jSpaceBet]} >
                            <PasscodeTextInput
                                autoFocus={true}
                                inputRef={(r) => { this.passcode1 = r }}
                                value={this.state.Otp1}
                                onChangeText={(value) => {
                                    this.setState({ Otp1: value }, () => {
                                    })
                                }}
                                onChange={(event) => { event && this.passcode2.focus() }}
                            />

                            <PasscodeTextInput
                                inputRef={(r) => { this.passcode2 = r }}
                                value={this.state.Otp2}
                                onChange={(event) => { event && this.passcode3.focus() }}
                                onChangeText={(value) => { this.setState({ Otp2: value }) }}
                            />

                            <PasscodeTextInput
                                inputRef={(r) => { this.passcode3 = r }}
                                value={this.state.Otp3}
                                onChange={(event) => { event && this.passcode4.focus() }}
                                onChangeText={(value) => { this.setState({ Otp3: value }) }}
                            />
                            <PasscodeTextInput
                                inputRef={(r) => { this.passcode4 = r }}
                                value={this.state.Otp4}
                                // onChange={(event) => { event && this.passcode5.focus() }}
                                onChangeText={(value) => { this.setState({ Otp4: value }) }}
                            />
                            {/* <PasscodeTextInput
                                inputRef={(r) => { this.passcode5 = r }}
                                value={this.state.Otp5}
                                onChange={(event) => { event && this.passcode6.focus() }}
                                onChangeText={(value) => { this.setState({ Otp5: value }) }}
                            />

                            <PasscodeTextInput
                                value={this.state.Otp6}
                                inputRef={(r) => { this.passcode6 = r }}
                                onChangeText={(value) => { this.setState({ Otp6: value }) }}
                            /> */}
                        </View>
                    </View>
                    <CButton onPress={() => this.onSubmit()} cStyle={[Styles.bgGrn, Styles.marV20, { height: 40, borderRadius: 40 }]} >
                        <CText cStyle={[Styles.cFFF, Styles.f16]} >VERIFY</CText>
                    </CButton>
                    <CButton onPress={() => this.resendOtp()} cStyle={[Styles.bgGrn, Styles.marV20, { height: 40, borderRadius: 40 }]} >
                        <CText cStyle={[Styles.cFFF, Styles.f16]} >Resend OTP</CText>
                    </CButton>
                </View>
            </View>
        );
    }
}