import React, { Component } from 'react';
import { View, AsyncStorage, Image, TouchableOpacity } from 'react-native';
import SocketIOClient from 'socket.io-client';
import { GiftedChat } from 'react-native-gifted-chat';
import { Styles, CardView, CText, CButton } from '../common';
const USER_ID = '@userId';

export default class PoolChat extends Component {

    constructor(props) {
        super(props);
        this.state = {
            messages: [],
            userId: null
        };

        
        this.determineUser = this.determineUser.bind(this);
        this.onReceivedMessage = this.onReceivedMessage.bind(this);
        this.onSend = this.onSend.bind(this);
        this._storeMessages = this._storeMessages.bind(this);

        this.socket = SocketIOClient('http://13.232.93.143:3000');
        this.socket.emit('init-channel', { poolId: '2' })
        this.socket.on('message-channel', this.onReceivedMessage);
        this.determineUser();
    }
    componentWillMount(){
        if(this.props.navigation.state.params.data){
            console.warn(this.props.navigation.state.params.data);
        }
    }
    /**
     * When a user joins the chatroom, check if they are an existing user.
     * If they aren't, then ask the server for a userId.
     * Set the userId to the component's state.
     */
    determineUser() {
        AsyncStorage.getItem(USER_ID)
            .then((userId) => {
                // If there isn't a stored userId, then fetch one from the server.
                if (!userId) {
                    this.socket.emit('userJoined', null);
                    this.socket.on('userJoined', (userId) => {
                        AsyncStorage.setItem(USER_ID, userId);
                        this.setState({ userId });
                    });
                } else {
                    this.socket.emit('userJoined', userId);
                    this.setState({ userId });
                }
            })
            .catch((e) => alert(e));
    }

    // Event listeners
    /**
     * When the server sends a message to this.
     */
    onReceivedMessage(messages) {
        console.warn("messages", messages.message)
        this._storeMessages(messages.message);
    }

    /**
     * When a message is sent, send the message to the server
     * and store it in this component's state.
     */
    onSend(messages = []) {
        this.socket.emit('message-channel', { poolId: '2', message: messages[0] });
        this._storeMessages(messages);
    }

    renderCamera() {

    }

    render() {
        var user = { _id: this.state.userId || -1 };

        return (
            <View style={[Styles.flex1]}>
                <Image style={[{ flex: 1, width: null, height: null }]} source={require('../images/dottedimage.png')} />
                <View style={[Styles.posAbsolute]}>
                    <View style={[{ flex: 0.3 }, Styles.bgFFF]}>
                        <View style={[Styles.row, Styles.mTop40, Styles.m20, Styles.jSpaceBet]}>
                            <View style={[Styles.row]} >
                                <TouchableOpacity onPress={() => { this.props.navigation.goBack() }}>
                                    <Image style={[Styles.mRt10, Styles.img30, Styles.brdRad15]} source={require('../images/icons/add-green.png')} />
                                </TouchableOpacity>
                                <View>
                                    <CText cStyle={[Styles.fWbold]}>Name</CText>
                                    <CText>Description</CText>
                                </View>
                            </View>
                            <View>
                                <CButton cStyle={[Styles.padV5, Styles.padH10, Styles.grnBtn]}>
                                    <CText cStyle={[Styles.cGrn]}>Follow</CText>
                                </CButton>
                            </View>
                        </View>
                    </View>
                    <View style={[{ height: 50 }, Styles.row, Styles.aitCenter,Styles.jSpaceBet, Styles.bgGrn72]}>
                        <CText cStyle={[Styles.m15, Styles.cFFF]}>Poolers</CText>
                        <TouchableOpacity onPress={() => { }}>

                        <Image source={require('../images/drop-down-icon.png')} />
                        </TouchableOpacity>

                    </View>
                    <GiftedChat
                        messages={this.state.messages}
                        onSend={this.onSend}
                        user={user}
                        alwaysShowSend={true}
                        renderActions={() => {
                            return <TouchableOpacity style={{ margin: 10 }}>
                                <Image source={require('../images/icons/camera-green.png')} style={{ width: 30, height: 30, resizeMode: 'contain' }} />
                            </TouchableOpacity>
                        }}
                    // renderSend={()=>{
                    //   return <TouchableOpacity style={{margin:10}}>
                    //     <Image source={require('./src/images/icons/send-green.png')} style={{ width: 30, height: 30, resizeMode: 'contain'}} />
                    //   </TouchableOpacity>
                    // }}
                    />
                </View>

            </View>
        );
    }

    // Helper functions
    _storeMessages(messages) {
        this.setState((previousState) => {
            return {
                messages: GiftedChat.append(previousState.messages, messages),
            };
        });
    }
}
