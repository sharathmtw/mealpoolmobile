import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity, Alert, FlatList, ScrollView } from 'react-native';
import { Styles, Header, CText, CardView, CButton } from '../common';
import Utils from '../common/Utils';

export default class myPools extends Component {
    state = {
        tabsBool: true,
    }
    render() {
        return (
            <View style={[Styles.flex1, Styles.bgFFF]} >
                <View style={[Styles.bgGrn, Styles.padH40, Styles.padV20, Styles.jSpaceArd, { height: 80 }]} >
                    <TouchableOpacity style={[]} onPress={() => { this.props.navigation.goBack() }} >
                        <Image style={[{ width: 25, height: 25 }]} source={require('../images/Arrow_Circle_left_white.png')} />
                    </TouchableOpacity>
                </View>
                <View style={[Styles.row, Styles.bgGrn]} >
                    <TouchableOpacity style={[this.state.tabsBool?[{borderBottomWidth:7,borderBottomColor:'#FFF'}]:null,]} onPress={()=>{this.setState({tabsBool:true})}} >
                        <CText cStyle={[this.state.tabsBool?Styles.fWbold:null, Styles.marH30,Styles.marV20, Styles.f20, Styles.cFFF]} >Pools</CText>
                    </TouchableOpacity>
                    <TouchableOpacity style={[!this.state.tabsBool?[{borderBottomWidth:7,borderBottomColor:'#FFF'}]:null,]} onPress={()=>{this.setState({tabsBool:false})}} >
                        <CText cStyle={[!this.state.tabsBool?[Styles.fWbold]:null, Styles.marH30,Styles.marV20,Styles.f20, Styles.cFFF]} >Requests</CText>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}