import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity, Alert, FlatList } from 'react-native';
import { Styles, Header, CText, CardView } from '../common';
import Utils from '../common/Utils';
import FooterTabs from '../common/FooterTabs';

export default class Homepage extends Component {
    state = {
        fameData: Utils.getStaticData('fameData'),
    }
    renderHashtags(item) {
        return <TouchableOpacity onPress={() => { }}>
            <CText cStyle={[Styles.m15, Styles.cFFF]}>{item.tagName}</CText>
        </TouchableOpacity>
    }
    renderFameData(item) {
        return <CardView cStyle={[Styles.p20]} hashNBtnBool={true} hashTag="#helloworld">
            <CText cStyle={[Styles.fWbold, Styles.f16, Styles.mTop5]}>{item.heading}</CText>
            <CText cStyle={[Styles.f16, Styles.mTop5]}>{item.fPoints}</CText>
        </CardView>
    }

    render() {
        return (
            <View style={[Styles.flex1, Styles.bgFFF]}>
                <Header
                    picVisible={true}
                    title="Fame"
                    notifVisible={true}
                />
                <View style={[Styles.flex1]}>
                    <FlatList
                        data={this.state.fameData}
                        keyExtractor={(item, iVal) => iVal.toString()}
                        renderItem={({ item, index }) => this.renderFameData(item, index)}
                        extradata={this.state}
                    />
                </View>
                <FooterTabs 
                homeClick={()=>{this.props.navigation.navigate('homepage')}}
                addClick={()=>{this.props.navigation.navigate('add',{screenName:'fame'})}}
                />
            </View>
        );
    }
}
