import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity, Alert, FlatList } from 'react-native';
import { Styles, Header, CText, CardView, CButton, CInput } from '../common';
import Utils from '../common/Utils';
import ImagePicker from 'react-native-image-picker';

const imgPickerOptions = {
    title: 'Select image', storageOptions: { skipBackup: true, path: 'images' },
    maxWidth: 1024, maxHeight: 512, quality: 1, noData: false
};

export default class addProfileInfo extends Component {
    state = {
        bio: '',
        viewImg: { uri: 'test' },
        sendImg: '',
        profileImgBool: false,
    }
    choosePic() {
        const self = this;
        ImagePicker.showImagePicker(imgPickerOptions, response => {
            console.warn(response)
            if (!response.didCancel && !response.error) {
                // tempImgArr.push(response.data);
                // tempImgRender.push({ uri: response.uri });
                self.setState({ imageUploadBool: true, sendImg: response.data, viewImg: { uri: response.uri }, profileImgBool: true });
            } else if (response.didCancel) {
                self.setState({ imageUploadBool: false });
            } else {
                alert('Could not select image');
            }
        })
    }
    onSubmit(){
        if(!this.state.profileImgBool){
            alert("Please Add Profile Picture");
        }else if(!this.state.bio && this.state.bio === ''){
            alert("Please add something about you");
        }else{
            this.props.navigation.navigate('homepage');
        }
    }
    render() {
        return (
            <View style={[Styles.flex1, Styles.bgWhite]} >
                <TouchableOpacity onPress={() => this.choosePic()} style={[Styles.aslCenter, Styles.mTop40, Styles.mBtm20, Styles.jCenter, { width: 140, height: 140, borderRadius: 140, backgroundColor: '#F6F6F6' }]}>
                    <View style={[{display: !this.state.profileImgBool ? 'flex' : 'none'}]} >
                        <CText cStyle={[Styles.aslCenter, { color: '#9B9B9B' }]} >Add</CText>
                        <CText cStyle={[Styles.aslCenter, { color: '#9B9B9B' }]} >Profile</CText>
                        <CText cStyle={[Styles.aslCenter, { color: '#9B9B9B' }]} >Picture</CText>
                    </View>
                    <Image style={[Styles.aslCenter,{ width: 140, height: 140, borderRadius: 140, display: this.state.profileImgBool ? 'flex' : 'none' }]} source={this.state.viewImg} />
                </TouchableOpacity>
                <View style={[Styles.padH20, Styles.aitStart, { borderTopWidth: 1, borderBottomWidth: 1, borderColor: '#F6F6F6' }]} >
                    <CText cStyle={[Styles.cBlk, Styles.mTop10]} >Write a short bio</CText>
                    <CInput
                        cStyle={[Styles.aslStretch, { height: 160 }]}
                        value={this.state.bio}
                        onChangeText={(value) => { this.setState({ bio: value }) }}
                        multiline={true}
                    />
                </View>
                <CButton onPress={() => this.onSubmit()} cStyle={[Styles.bgGrn, Styles.marH40, Styles.mTop30, { height: 40, borderRadius: 40 }]} >
                    <CText cStyle={[Styles.cFFF, Styles.f16]} >SAVE</CText>
                </CButton>
                <TouchableOpacity style={[Styles.mTop10]} onPress={() => { this.props.navigation.navigate('homepage') }} >
                    <CText cStyle={[Styles.cBlk, Styles.aslCenter]} >Skip</CText>
                </TouchableOpacity>
            </View>
        );
    }
}