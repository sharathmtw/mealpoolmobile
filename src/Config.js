const routes = {
    base:'http://13.232.93.143:3000',
    //eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViNmQyZjc5YTdmNTk1MDhkZDIwM2Y5OSIsInBob25lIjo4NjM5NTE5MTUxLCJuYW1lIjoic2hhcmF0aCIsImNvbXBhbnlJZCI6IjViNmQyZDU2N2U3Y2YyMDc4ODhlOGU3NyIsImlhdCI6MTUzMzg4MjMxOX0.nS2rlc4Uez0WII2XBCJIRD1MMomIz43bdLOE5VZFWUE
    getAllActivities: '/pool/getAllActivities',
    verifyOtp: '/user/checkOtp',
    registerCompany:'/user/registerCompany',
    registerUser: '/user/registerUser',
    loginUser: '/user/login/', //:number
    verifyOtp: '/user/checkOtp',
    checkCommpanyCode: '/user/checkCommpanyCode',
    //pool
    addPool:'/pool/addPool',
    addRequest:'/pool/addRequest',

}
const appUtils = {
    passwordLength: 6,
    fullMonthName: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    shortMonthName: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    weekDays: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
    currentDate: new Date(),
    deviceId: 'VG05M1VHeGhlVEV5TTBSbGRtbGpaVUYxZEdnPQ=='
};

export default { routes, appUtils };