import React from 'react';
import { View, TextInput, Platform } from 'react-native';
import { Styles } from './index';

const CInput = ({ cStyle, keyboardType, onChangeText, placeholder, multiline, uColor, pColor, editable, value, secureTextEntry, maxLength }) => {
    return (
        <TextInput style={[Styles.inputStyle, Styles.cBlk, Styles.ffLt, cStyle, Platform.OS === 'ios' ? Styles.profileTabs : {}]} onChangeText={onChangeText}
            keyboardType={keyboardType} placeholder={placeholder} multiline={multiline || false} value={value}
            underlineColorAndroid={uColor} placeholderTextColor={pColor} editable={editable}
            secureTextEntry={secureTextEntry} maxLength={maxLength} autoCapitalize="none" autoCorrect={false} spellCheck={false}
        />
    );
};

export { CInput };