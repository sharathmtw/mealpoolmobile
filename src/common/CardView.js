import React from 'react';
import { View, Platform, Image, Dimensions, TouchableOpacity } from 'react-native';
import { Styles, CText, CButton } from './index';

const CardView = ({ children, cStyle, hashTag, hashNBtnBool, imgClick, imgSource }) => {
    return (
        <View>
            <View style={[cStyle, Styles.row, Styles.bgEEE, , Styles.bgFFF]}>
                <TouchableOpacity onPress={imgClick}>
                    <Image source={imgSource} style={[Styles.img40, Styles.m10, Styles.brdRad20]} />
                </TouchableOpacity>
                <View style={[localStyles.cardWidth, Styles.padV5, Styles.padH5]}>
                    {children}
                </View>
                <View style={[{ display: hashNBtnBool ? "none" : "flex" }, Styles.aitStart, localStyles.hashWidth, Styles.mRt5, Styles.marV10]}>
                    <CText>{hashTag}</CText>
                </View>
                <View style={[Styles.aitStart, localStyles.hashWidth, Styles.mRt5, Styles.marV10]}>
                    <CButton cStyle={[{ display: hashNBtnBool ? "flex" : "none" }, Styles.padV5, Styles.padH10, Styles.grnBtn]}>
                        <CText cStyle={[Styles.cGrn]}>Follow</CText>
                    </CButton>
                </View>
            </View>
            <View style={[Styles.row,{height:3,backgroundColor:'red'}]} >
                <View style={[{flex:2,backgroundColor:'#C8E6CE'}]} >

                </View>
                <View style={[{flex:2,backgroundColor:'#F6F6F6'}]} >

                </View>
            </View>
        </View>
    );
};

const localStyles = {
    cardWidth: {
        width: Dimensions.get('window').width * 3 / 5,
    },
    hashWidth: {
        width: (Dimensions.get('window').width * 2 / 5) - 65,
    }
}

export { CardView };