import React from 'react';
import { View, TouchableOpacity, Image} from 'react-native';
import { CText, Styles } from '../common/index';

const CCheckBox = ({ label, onPress, activeStyle, cStyle }) => {

    function changeCheckboxStatus(){
        if(activeStyle){
            return <Image style={{width:20, height:20}} source={require('../images/tick-icon.png')} />
        } else {
            return;
        }
    }

    return (
        <TouchableOpacity onPress={onPress} style={[Styles.row, [cStyle]]}>
            <View style={[Styles.checkboxStyle]}>
                {/* <View style={[Styles.checkboxActiveStyle, [activeStyle]]}></View> */}
                {changeCheckboxStatus()}
            </View>
            <CText cStyle={[Styles.mLt10, Styles.mTop3]}>{label}</CText>
        </TouchableOpacity>
    );
};

export { CCheckBox };