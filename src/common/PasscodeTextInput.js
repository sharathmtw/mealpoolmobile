import React from 'react';
import { View, TextInput, Platform } from 'react-native';
import { Styles } from './index';

const PasscodeTextInput = ({ inputRef,onChangeText,value,onChange }) => {
    return (
      <View>
        <TextInput
          ref={(r) => { inputRef && inputRef(r) }}
          maxLength={1}
          style={[{borderBottomWidth:3,borderRadius:2,width:30}]}
          value={value}
          onChangeText={onChangeText}
          onChange={value===''?onChange:null}
        />
      </View>
    );
  }

  export { PasscodeTextInput };