import React from 'react';
import { View, Picker, Modal, Image, TouchableOpacity } from 'react-native';
import { CText, Styles } from '.';

const CPicker = ({ children, pickerSelectedValue, mVisible, onPress, fadePress, cStyle }) => {
    return (
        <View style={[Styles.flex1, Styles.bgEEE, cStyle]}>
            {/* <Picker style={[styles.pickerStyle, styles.c333, cStyle]} selectedValue={value} onValueChange={onChange}>
                
            </Picker> */}
            <TouchableOpacity onPress={onPress} style={[Styles.pickerStyle, Styles.row, Styles.jSpaceBet, Styles.aitCenter, Styles.padH10]}>
                <CText>{pickerSelectedValue}</CText>
                <Image style={{ width: 30, height: 30, resizeMode: 'contain' }} source={require('../images/drop-down-icon.png')} />
            </TouchableOpacity>
            <Modal visible={mVisible} animationType={'slide'} onRequestClose={() => {}}>
                <TouchableOpacity style={Styles.popContainer} onPress={fadePress}></TouchableOpacity>
                {children}
            </Modal>
        </View>
    );
};

export { CPicker };