import React from 'react';
import { AsyncStorage } from 'react-native';
import axios from 'axios';
import _ from 'underscore';
import Config from '../Config';

var Utils = function () { };

Utils.prototype.isValidMobile = function (phoneNumber) {
    return phoneNumber && _.isNumber(phoneNumber) && (phoneNumber.toString().length === 10);
};

Utils.prototype.isValidEmail = function (email) {
    return email && /^\S+@\S+\.\S+/.test(email);
};

Utils.prototype.isValidNum = function (value) {
    return value && /^[0-9]*$/.test(value);
};

Utils.prototype.isValidPassword = function (pwd) {
    return _.isString(pwd) && (pwd.length >= Config.appUtils.passwordLength);
};

Utils.prototype.getToken = function (key, callBack) {
    AsyncStorage.getItem('mealpool:' + key, (err, resp) => {
        if (err)
            callBack('Error fetching token', false);
        else
            callBack(JSON.parse(resp), true);
    });
};

Utils.prototype.setToken = function (key, value, callBack) {
    AsyncStorage.setItem('mealpool:' + key, JSON.stringify(value), (err) => {
        if (err)
            callBack('Error setting token', false);
        else
            callBack(null, true);
    });
};

Utils.prototype.getStaticData = function (value) {
    if (value === 'hashtags') {
        return [
            { id: 1, tagName: '#Lunch' },
            { id: 2, tagName: '#Snack' },
            { id: 3, tagName: '#BreakFast' },
            { id: 4, tagName: '#Dinner' },
        ];
    }
    else if (value === 'poolData') {
        return [
            { id: 1, host:{name:''},desp: "fjahd ajsdh ajdh adjf asdjahsdfdk fahdfa dfkajhsdhf adh jdhafl sdj", tag: "#Snack", heading: 'Lunch' },
            { id: 2, host:{name:''},desp: "fjahd ajsdh ajdh adjf asdjahsdfdk fahdfa dfkajhsdhf adh jdhafl sdj", tag: "#Snack", heading: 'Snack' },
            { id: 3, host:{name:''},desp: "fjahd ajsdh ajdh adjf asdjahsdfdk fahdfa dfkajhsdhf adh jdhafl sdj", tag: "#Lunch", heading: 'Dinner' },
        ];
    }
    else if (value === 'fameData') {
        return [
            { id: 1, fPoints: "450 Fame", heading: 'Prabat Kumar' },
            { id: 2, fPoints: "500 Fame", heading: 'Jayadeep Reddy' },
            { id: 3, fPoints: "550 Fame", heading: 'Raju Bandari' },
            { id: 3, fPoints: "600 Fame", heading: 'Chaitanya Degala' },
        ];
    }
    else if (value === 'notifData') {
        function addZero(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }
        var d = new Date();
        var h = addZero(d.getHours());
        var m = addZero(d.getMinutes());
        var s = addZero(d.getSeconds());
        var time = h + ":" + m + ":" + s;
        return [
            { id: 1, message: "fjahd ajsdh ajdh adjf asdjahsdfdk fahdfa dfkajhsdhf adh jdhafl sdj", timeStamp: time },
            { id: 2, message: "fjahd ajsdh ajdh adjf asdjahsdfdk fahdfa dfkajhsdhf adh jdhafl sdj", timeStamp: time },
            { id: 3, message: "fjahd ajsdh ajdh adjf asdjahsdfdk fahdfa dfkajhsdhf adh jdhafl sdj", timeStamp: time },
            { id: 3, message: "fjahd ajsdh ajdh adjf asdjahsdfdk fahdfa dfkajhsdhf adh jdhafl sdj", timeStamp: time },
            { id: 3, message: "fjahd ajsdh ajdh adjf asdjahsdfdk fahdfa dfkajhsdhf adh jdhafl sdj", timeStamp: time },
        ];
    }
    else if (value === 'tabs') {
        return [
            { id: 1, tabName: 'Pools', status: true },
            { id: 2, tabName: 'Requests', status: false },
        ]
    }
    else if(value === 'mealTypes'){
        return [
            {id:1,name:'Breakfast',selected:false},
            {id:2,name:'Lunch',selected:false},
            {id:3,name:'Snack',selected:false},
            {id:4,name:'Dinner',selected:false},
        ];
    }
}

export default new Utils();