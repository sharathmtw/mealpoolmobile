import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {Styles} from './index';

const CButton = ({children, cStyle, onPress}) => {
    return (
        <TouchableOpacity style={[Styles.jCenter, Styles.aitCenter, cStyle]} onPress={onPress}>
            {children}
        </TouchableOpacity>
    );
};

export {CButton};