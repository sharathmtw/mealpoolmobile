import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Dimensions, ScrollView, FlatList } from 'react-native';
import { CText, Styles, CSpinner, CInput, CRadio, CCheckBox, Header } from '../common/index';
import Utils from '../common/Utils';

export default class FooterTabs extends Component {
    state = { filterType: '' }

    getHomeImage() {
        if (this.props.categoryBool) {
            return <Image style={Styles.btmMenuImg} source={require('../images/icons/home-green.png')} />;
        } else {
            return <Image style={Styles.btmMenuImg} source={require('../images/icons/home-green.png')} />;
        }
    }

    getAddImage() {
        if (this.props.categoryBool) {
            return <Image style={Styles.btmCenterMenuImg} source={require('../images/icons/plus-green.png')} />;
        } else {
            return <Image style={Styles.btmCenterMenuImg} source={require('../images/icons/plus-green.png')} />;
        }
    }
    getFameImage() {
        if (this.props.categoryBool) {
            return <Image style={Styles.btmMenuImg} source={require('../images/icons/fame-grey.png')} />;
        } else {
            return <Image style={Styles.btmMenuImg} source={require('../images/icons/fame-grey.png')} />;
        }
    }
    render() {
        const { homeClick, addClick, paperDollClick, notifClick, fameClick, categoryBool, homeActive, paperDollActive, paperDollCount } = this.props;
        return (
            <View style={[Styles.brdr, Styles.row, Styles.jCenter,Styles.aitCenter, Styles.bgWhite, Styles.padV8, { position: 'absolute', bottom: 0, left: 0, right: 0, borderTopWidth: 1, borderTopColor: '#EEE' }]}>
                <TouchableOpacity onPress={homeClick} style={[Styles.aitCenter]}>
                    {this.getHomeImage()}
                    <CText cStyle={[, Styles.f12, { color: homeActive || 'grey' }]}>Mealpool</CText>
                </TouchableOpacity>
                <TouchableOpacity onPress={addClick} style={[Styles.aitCenter, Styles.marH40]}>
                    {this.getAddImage()}
                </TouchableOpacity>
                <TouchableOpacity onPress={fameClick} style={[Styles.aitCenter]}>
                    {this.getFameImage()}
                    <CText cStyle={[, Styles.f12]}>Fame</CText>
                </TouchableOpacity>
            </View>

        );
    }
}


