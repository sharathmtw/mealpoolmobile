import React from 'react';
import { View, Modal, TouchableOpacity, Image } from 'react-native';
import { CText, CButton, Styles } from '../common/index';

const Header = ({ title, picVisible,picSource, backBtnClick, logoVisible, categoryColor, savedItemsClick, skipOnPress,
    searchVisible, filterVisible, notifVisible, arrowVisible, picClick, searchClick, filterClick, notifCount, savedItemsCount, notifClick }) => {
    return (
        <View style={[Styles.aitCenter, Styles.jSpaceBet, Styles.headContainer, { backgroundColor: categoryColor || '#F6F6F6', zIndex: 1 }]}>
            <View style={[Styles.row, { width: 80 }]}>
                <TouchableOpacity style={{ display: picVisible ? 'flex' : 'none' }} onPress={picClick}>
                {/**************temporary size back color **********************/}
                    <Image style={[Styles.jStart, Styles.marH10, Styles.headerImg, Styles.img40, Styles.brdRad20, Styles.bgFFF]} source={picSource} />
                </TouchableOpacity>
                <TouchableOpacity style={{ display: arrowVisible ? 'flex' : 'none' }} onPress={backBtnClick}>
                    <Image style={[Styles.jStart, Styles.m5, Styles.headerImg]} source={require('../images/arrow-back-white.png')} />
                </TouchableOpacity>
            </View>
            <View>
                {<TouchableOpacity style={{ display: logoVisible ? 'flex' : 'none' }} onPress={() => { }}>
                    <Image style={[Styles.jStart, Styles.m5, { width: 60, height: 60, resizeMode: 'contain' }]} source={require('../images/logo-white.png')} />
                </TouchableOpacity>}
                <CText cStyle={[Styles.c222, Styles.f18]}>{title}</CText>
            </View>
            <View style={[Styles.row, { width: 80 }, Styles.jEnd]}>
                <TouchableOpacity style={{ display: searchVisible ? 'flex' : 'none' }} onPress={searchClick}>
                    <Image style={[Styles.jEnd, Styles.marH5, Styles.marV5, Styles.headerImg]} source={require('../images/icons/search-orange.png')} />
                </TouchableOpacity>
                <TouchableOpacity style={{ display: notifVisible ? 'flex' : 'none' }} onPress={notifClick}>
                    <Image style={[Styles.aslEnd, Styles.marH5, Styles.m5, Styles.headerImg]} source={require('../images/icons/notification-bell-green.png')} />
                    <View style={[Styles.bgFFF, Styles.headerCounts, Styles.jCenter, Styles.aitCenter]}>
                        <CText cStyle={[Styles.c111, Styles.f12, Styles.txtAlignCen]}>{notifCount}</CText>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export { Header };