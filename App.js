import React, { Component } from 'react';
import { Platform } from 'react-native';
import Router from './src/Router';
import SplashScreen from 'react-native-splash-screen'

export default class App extends Component {
  componentWillMount() {
    if (Platform.OS === 'android') {
      SplashScreen.hide();
    }
  }
  render() {
    return (
      <Router />
    );
  }

}